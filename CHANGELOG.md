# Petrified Changelog

Project Home: https://gitlab.com/troyengel/petrified

## [2.0.3] - 2019-10-04
- Update systemd units to use Requires (#6)

## [2.0.2] - 2018-06-05
- Migrate to gitlab.com and update README / man page
- Add basic CONTRIBUTING.md

## [2.0.1] - 2017-11-19
- Add `CHANGELOG.md` to project
- Update README / man for new systemd things
- Rebuild systemd again to use timers.target (#4)
  - Remove petrified.target design completely
  - Instanced units can be created manually using systemd overrides
- Use templates to build systemd units ExexStart path (#4)
  - Unit files to install are now templates built by Makefile
  - Path is determined by `PREFIX=` semantics (packaging standard)
- Readjust copyright date, US Copyright Law is first year published

## [2.0.0] - 2017-11-19
- Implement FreeDNS APIv2 alongside APIv1 with dynamic API guess (#5)
  - Default config is to guess which API based on key length
  - Specific API version can be coded into config, see `DDNS_API`
  - See comments in `petrified.conf` for details on new settings
- Add new `LOG_QUIET` option to not emit if IP hasn't changed (#4)
- Update shebang to be BSD portable (bin/bash -> bin/env bash)

## [1.0.6] - 2016-01-24
- Rebuild systemd design for timers and target (#3)
- Fix systemd `OnCalendar` implementation
- Add systemd `OnActiveSec` to timer on startup
- Deprecate Arch build - AUR started using git
- Update / enhance docs and man page

## [1.0.5] - 2015-03-08
- Fix bad perlpod char
- Add cleanup routine to bad IP exit clause
- Update / enhance Makefile

## [1.0.4] - 2014-12-01
- Fix bad month macro in logfile entries (@IanMReed)

## [1.0.3] - 2014-01-24
- Add Makefile to project
- Convert `README.md` to `README.pod` for multi-use
- Add petrified.1 man page built from README.pod
- Update / enhance Arch `PKGBUILD` things
- Change logrotate to not install systemwide

## [1.0.2] - 2014-11-22
- Documentation updates and enhancements
- First addition of systemd timers (#1)
- Reorganize file layout (move things to subdirs)
- Add better examples for (f)cron, logrotate, etc.
- Add security / permissions check of config file
  - Contains API key, should be kept protected

## [1.0.1] - 2014-09-07
- First release of working version
- First tagged release for packagers

## [1.0.0] - unreleased / untagged
- First commits to build project

