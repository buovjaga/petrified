###########################################################################
## petrified configuration file
#
# This is the configuration for petrified - a client for updaing dynamic
# DNS at freedns.afraid.org. The global /etc/petrified.conf variables can
# be overridden on a per-user basis via ~/.petrifiedrc as well.
#
# Passing '-c <config>' on the commandline will read *only* the named
# config file, ignoring /etc/petrified.conf and ~/.petrifiedrc entirely.
# For best security of the DDNS_KEY, ensure the config file is mode 0600
# and owned by the user in question.
#
# See petrified.crontab for examples on how to run via cron/fcron.


# This is the unique key for the dynamic DNS entry to update; visit:
#   https://freedns.afraid.org/dynamic/    (v1 API) or
#   https://freedns.afraid.org/dynamic/v2/ (v2 API)
# Next to the DNS entries towards the bottom is a link to afraid.org;
# copy that and extract the string and put here, this is your API key.
#
# Example v1: DDNS_KEY=9C4yjq3kaekkYNbYsKRqcosTmouMYbfVMLkJtR==
# Example v2: DDNS_KEY=ZIfSWnXKuDqMpNEsTXBA852
DDNS_KEY=

# The migration from API v1 to API v2 requires the user to visit the
# website and click a few buttons to generate a new API key for each
# domain; the API v1 length is variable but API v2 is a fixed length
# at 24 chars. This setting will try and autodetect the API version by
# the length of DDNS_KEY - if 24, use v2 else use v1.
# 0 = auto
# 1 = force API v1
# 2 = force API v2
DDNS_API=0

# Which URL to use to get an IP - the expectation is output from this
# host in plain text, just the IP - no parsing will be done. The
# icanhazip service returns the IPv4 and IPv6 nice and clean.
#
# IPv4 icanhazip (http://major.io/icanhazip-com-faq/)
DDNS_CHECK="http://4.icanhazip.com"
#
# IPv6 icanhazip (http://major.io/icanhazip-com-faq/)
#DDNS_CHECK="http://6.icanhazip.com"

# How many seconds should curl wait when either checking your IP or
# trying to update the remote DNS {<integer>}
#
#CURL_WAIT=10

# Report an internal IP instead of the public IP - useful if you're
# using dynamic DNS for machines on the internal network {0,1}
#
#LOCAL_MODE=0

# Specify which interface {<string>}
#
#LOCAL_IF=eth0

# Should we use IPv4 or IPv6 {4,6}
#
#LOCAL_IV=4

# How to log - multiple supported {0,1}
#
#USE_JOURNAL=1
#USE_SYSLOG=0
#USE_STDOUT=0
#USE_LOGFILE=0

# If set, does not log if the IP has not changed {0,1}
#
#LOG_QUIET=0

# If USE_LOGFILE is 1, where to log {<string>}
#
#PET_LOG=/var/log/petrified.log

# Prevent race conditions, i.e. stuck crons piling up {0,1}, {<string>}
#
#USE_PID=1
#PET_PID=/run/petrified.pid

# Save the IP from our last check {0,1}, {<string>}
#
#USE_LIP=1
#PET_LIP=/var/cache/petrified/lastip.dat

