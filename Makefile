# petrified - a bash client to update dynamic DNS at freedns.afraid.org

VERSION := $(shell grep '^\#\# Version:' petrified | cut -d' ' -f3)

PREFIX    ?= /usr/local
BINPREFIX ?= $(PREFIX)/bin
LIBPREFIX ?= $(PREFIX)/lib
MANPREFIX ?= $(PREFIX)/share/man
DOCPREFIX ?= $(PREFIX)/share/doc
ETCPREFIX ?= /etc
VARPREFIX ?= /var
DESTDIR   ?=

MANPAGES = \
	petrified.1

all: doc build-systemd

doc: $(MANPAGES)
petrified.1: README.pod
	pod2man -s 1 -c "Petrified Manual" -n "PETRIFIED" \
		-r "petrified $(VERSION)" $< $@

install: all install-main install-doc
uninstall: uninstall-main uninstall-doc

install-main:
	install -Dm0755 petrified "$(DESTDIR)$(BINPREFIX)/petrified"
	install -Dm0644 petrified.1 "$(DESTDIR)$(MANPREFIX)/man1/petrified.1"
	install -Dm0600 petrified.conf "$(DESTDIR)$(ETCPREFIX)/petrified.conf"
	install -dm0755 "$(DESTDIR)$(VARPREFIX)/cache/petrified"

install-doc:
	install -dm0755 "$(DESTDIR)$(DOCPREFIX)/petrified/extra"
	install -Dm0644 README.pod LICENSE CHANGELOG.md \
		"$(DESTDIR)$(DOCPREFIX)/petrified"
	install -Dm0644 extra/petrified.crontab extra/petrified.dispatch \
		extra/petrified.logrotate "$(DESTDIR)$(DOCPREFIX)/petrified/extra"

build-systemd:
	sed -e 's|@BINDIR@|$(BINPREFIX)|' systemd/petrified-system.service.in > \
		systemd/petrified-system.service
	sed -e 's|@BINDIR@|$(BINPREFIX)|' systemd/petrified-user.service.in > \
		systemd/petrified-user.service

clean-systemd:
	rm systemd/petrified-system.service systemd/petrified-user.service

install-systemd: build-systemd
	# system unit
	install -dm0755 "$(DESTDIR)$(LIBPREFIX)/systemd/system"
	install -Dm0644 systemd/petrified-system.service \
		"$(DESTDIR)$(LIBPREFIX)/systemd/system/petrified.service"
	install -Dm0644 systemd/petrified.timer \
		"$(DESTDIR)$(LIBPREFIX)/systemd/system/petrified.timer"
	# user unit
	install -dm0755 "$(DESTDIR)$(LIBPREFIX)/systemd/user"
	install -Dm0644 systemd/petrified-user.service \
		"$(DESTDIR)$(LIBPREFIX)/systemd/user/petrified.service"
	install -Dm0644 systemd/petrified.timer \
		"$(DESTDIR)$(LIBPREFIX)/systemd/user/petrified.timer"

uninstall-main:
	rm "$(DESTDIR)$(BINPREFIX)/petrified" \
		"$(DESTDIR)$(MANPREFIX)/man1/petrified.1" \
		"$(DESTDIR)$(ETCPREFIX)/petrified.conf"
	rmdir "$(DESTDIR)$(VARPREFIX)/cache/petrified"

uninstall-doc:
	rm "$(DESTDIR)$(DOCPREFIX)/petrified/README.pod" \
		"$(DESTDIR)$(DOCPREFIX)/petrified/LICENSE" \
		"$(DESTDIR)$(DOCPREFIX)/petrified/extra/petrified.crontab" \
		"$(DESTDIR)$(DOCPREFIX)/petrified/extra/petrified.dispatch" \
		"$(DESTDIR)$(DOCPREFIX)/petrified/extra/petrified.logrotate"
	rmdir "$(DESTDIR)$(DOCPREFIX)/petrified/extra" \
		"$(DESTDIR)$(DOCPREFIX)/petrified" \

uninstall-systemd:
	rm "$(DESTDIR)$(LIBPREFIX)/systemd/system/petrified.timer" \
		"$(DESTDIR)$(LIBPREFIX)/systemd/system/petrified.service"
	rm "$(DESTDIR)$(LIBPREFIX)/systemd/user/petrified.timer" \
		"$(DESTDIR)$(LIBPREFIX)/systemd/user/petrified.service"

clean: clean-systemd
	$(RM) $(MANPAGES)

.PHONY: clean doc install uninstall

